/********************************************************************
 * Copyright (C) 2015 Shanavas m <shanavas[dot]m2[at]gmail.com>
 *
 * This script is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 2 of the License, or (at your option) any later
 * version. See http://www.gnu.org/copyleft/gpl.html the full text of the
 * license.
 ********************************************************************/

var self = require("sdk/self");
var data = self.data;
var tabs = require('sdk/tabs');

var opacity = 0;

//one worker for each tab
var workers = [];

/**
 * when tab loaded, add worker to workers array
 * and intialize nightmod
 **/
tabs.on('ready', function(tab){
    var worker = tab.attach({
        contentScriptFile: data.url("brightness.js")
    });
    workers.push(worker);
    worker.test = tab.url;
    worker.on("detach", function() {
		var index = workers.indexOf(worker);
		if (index >= 0) workers.splice(index, 1);
	});

    worker.port.emit("init", opacity);
});

/**
 * update the changes made on other tabs
 * to the current tab
 **/
tabs.on('activate', function(tab) {
    var worker = getWorker();
    if(worker){
        worker.port.emit("update", opacity);
    }
});


//panel showing scroll input
var bright_input = require("sdk/panel").Panel({
    height: 50,
    contentURL: data.url("panel.html"),
    contentScriptFile: data.url("control.js")
});
/**
 * listen to control signal from panel and 
 * calls the brightness_changed function
 **/
bright_input.port.on("brightness_changed", brightness_changed);

/**
 * @function brightness_changed
 * update the opacity variable,
 * finds the correspoding worker of activated tab
 * and sends update signal to worker if any
 * 
 * @param op updated opacity opacity
 **/
function brightness_changed(op){
    opacity = op;
    var worker = getWorker();
    if(worker){
        worker.port.emit("update", opacity);
    }
}

// Create a button
var { ActionButton } = require("sdk/ui/button/action");
var button = ActionButton({
    id: "nighthawk-panel",
    label: "nighthawk",
    icon: {
        "16": "./icon-16.png",
        "32": "./icon-32.png",
        "64": "./icon-64.png"
    },
    onClick: handleClick
});

// Show the panel when the user clicks the button.
function handleClick(state) {
    bright_input.show({
        position: button
    });
}

/**
 * @function getWorker
 * 
 * finds the correspoding worker of 
 * active tab
 * 
 * @return worker
 **/
function getWorker(){
    for(var i=0; i<workers.length; i++){
        if(workers[i].tab === tabs.activeTab){
            return workers[i];
        }
    }
}

