Nighthawk
=========
A simple night mode for firefox browser.

Darkens the browser window for night browsing and protects your eyes.

