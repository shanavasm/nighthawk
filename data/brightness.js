/********************************************************************
 * Copyright (C) 2015 Shanavas m <shanavas[dot]m2[at]gmail.com>
 *
 * This script is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 2 of the License, or (at your option) any later
 * version. See http://www.gnu.org/copyleft/gpl.html the full text of the
 * license.
 ********************************************************************/

self.port.on("init", init);
self.port.on("update", update);
var div_id = null;

/**
 * @function init
 * 
 * initializes nightmode
 * create a division and styles appropriately
 * and appends to the document body
 **/
function init(opacity){
    //var zindex = opacity? "999": "-999";

    if(document.body){
        var control_div = document.createElement('div');
        control_div.style.position = "fixed";
        div_id = 'nighthawk_' + Math.floor(Math.random() * 999999);
        control_div.id = div_id;
        control_div.style.top = '0px';
        control_div.style.left = '0px';
        control_div.style.backgroundColor = 'black';
        control_div.style.opacity = opacity;
        control_div.style.zIndex = '-1';// zindex;
        control_div.style.width = '100%';
        control_div.style.height = '100%';
        document.body.appendChild(control_div);
    }
}

/**
 * @function update
 * 
 * updates the opacity and z-index of 
 * brightness control division
 *
 **/
function update(opacity){
    if(document.body){
        var control_div = document.getElementById(div_id);
        //var zindex = opacity? "999": "-999";
        control_div.style.opacity = opacity;
        //control_div.style.zIndex = zindex;
    }
}
