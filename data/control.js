/********************************************************************
 * Copyright (C) 2015 Shanavas m <shanavas[dot]m2[at]gmail.com>
 *
 * This script is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 2 of the License, or (at your option) any later
 * version. See http://www.gnu.org/copyleft/gpl.html the full text of the
 * license.
 ********************************************************************/

var input = document.getElementById("brightness");

input.addEventListener('change', function onkeyup(event) {
    var brightness = input.value;
    var opacity = 1 - (brightness/10);
    console.log('control op:'+opacity);
    self.port.emit("brightness_changed", opacity);
}, false);

